void WebRenderer::renderStyleSheet(WStringStream& out,
				   const WCssStyleSheet& sheet,
				   WApplication *app)
{
  // https://github.com/filamentgroup/loadCSS/
  out << "<script>(function() {"
      "var ss = document.createElement('link');"
      "var ref = document.getElementsByTagName('script')[0];"
      "ss.rel = 'stylesheet';"
      "ss.href = '";
  DomElement::htmlAttributeValue(out, sheet.link().resolveUrl(app));
  out << "';"
      "ss.media = 'only x';"
      "ref.parentNode.insertBefore(ss, ref);"
      "setTimeout(function(){ ss.media = '" << sheet.media() << "'; } );"
      "})()"
      "</script>";

  out << "<noscript>";
  out << "<link href=\"";
  DomElement::htmlAttributeValue(out, sheet.link().resolveUrl(app));
  out << "\" rel=\"stylesheet\" type=\"text/css\"";
  if (!sheet.media().empty() && sheet.media() != "all")
    out << " media=\"" << sheet.media() << '"';
  closeSpecial(out);
  out << "</noscript>";
}

