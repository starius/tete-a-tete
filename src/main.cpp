/******************************************
* Tete-a-tete, anonumous private chat     *
* Copyright (C) 2014 Boris Nagaev         *
* Tete-a-tete is licensed under           *
* the GNU GPL Version 2                   *
******************************************/

#ifdef MULTI_THREADED
#include "boost-xtime.hpp"
#include <boost/thread/mutex.hpp>
#define CHAT_MUTEX boost::mutex
#define CHAT_LOCK boost::mutex::scoped_lock
#else
#define BOOST_DISABLE_THREADS
#define CHAT_MUTEX int
struct DummyLock {
    DummyLock(const CHAT_MUTEX&) {
    }
};
#define CHAT_LOCK DummyLock
#endif

#include <csignal>
#include <fstream>
#include <sstream>
#include <vector>
#include <set>
#include <algorithm>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/foreach.hpp>
#include <boost/cast.hpp>
#include <boost/format.hpp>
#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/algorithm/string/case_conv.hpp>

class TaTApp;
// FIXME nasty public morozov
#define localizedStrings_ localizedStrings_;\
    friend class ::TaTApp;
#include <Wt/WApplication>
#undef localizedStrings_

#include <Wt/WCombinedLocalizedStrings>
#include <Wt/WEnvironment>
#include <Wt/WServer>
#include <Wt/WEnvironment>
#include <Wt/WHBoxLayout>
#include <Wt/WVBoxLayout>
#include <Wt/WStackedWidget>
#include <Wt/WBootstrapTheme>
#include <Wt/WContainerWidget>
#include <Wt/WTemplate>
#include <Wt/WScrollArea>
#include <Wt/WLineEdit>
#include <Wt/WSound>
#include <Wt/WImage>
#include <Wt/WAnchor>
#include <Wt/WLink>
#include <Wt/WBreak>
#include <Wt/Utils>
#include <Wt/WRandom>
#include <Wt/WText>
#include <Wt/WPushButton>
#include <Wt/WResource>
#include <Wt/WDateTime>
#include <Wt/WTime>
#include <Wt/WTimer>
#include <Wt/Http/Response>
#include <Wt/WJavaScript>
#include <Wt/WTable>
#include <Wt/WTableCell>
#include <Wt/WViewWidget>
#include <Wt/WCssDecorationStyle>

#include "GlobalLocalizedStrings.hpp"

using namespace Wt;
using namespace Wt::Wc;

#define TR(m) WString::tr(m)
#define TO_S(m) boost::lexical_cast<std::string>(m)
#define DO_JS(m) if (wApp->environment().ajax()) { \
                    doJavaScript(m); }
#define DOWNCAST boost::polymorphic_downcast

struct Message {
    WString text_;
    WDateTime time_;
    WApplication* author_;
};

WString timeOf(const Message& m);

WString colorOf(WApplication* app) {
    std::string sha1 = Utils::sha1(TO_S(app));
    sha1.resize(3);
    for (int i = 0; i < 3; i++) {
        sha1[i] |= 128;
    }
    return '#' + Utils::hexEncode(sha1);
}

bool isMobile();

typedef std::vector<Message> Messages;

typedef std::set<std::string> StringSet;
StringSet active_users_;
CHAT_MUTEX users_number_mutex_;

bool has_emotions_;

std::string approot_;

typedef boost::shared_ptr<GlobalLocalizedStrings> GLSPtr;
GLSPtr gls_;

struct Chat {
    Messages messages_;
    CHAT_MUTEX mutex_;
    WDateTime created_;
    std::vector<std::string> sessions_;
    WApplication* interrupter_;
    bool reserved_ : 1;
    bool multiuser_ : 1;

    Chat():
        created_(WDateTime::currentDateTime()),
        sessions_(2),
        interrupter_(0), reserved_(0), multiuser_(0) {
    }

    void join() {
        if (multiuser_) {
            if (sessions_.empty()) {
                messages_.clear();
            }
            sessions_.push_back(wApp->sessionId());
        } else {
            sessions_[1] = wApp->sessionId();
        }
    }

    void leave() {
        if (multiuser_) {
            std::string myId = wApp->sessionId();
            sessions_.erase(std::remove(sessions_.begin(),
                                        sessions_.end(), myId),
                            sessions_.end());
        } else {
            if (!interrupter_) {
                interrupter_ = wApp;
            }
        }
    }

    bool active() const {
        if (multiuser_) {
            return !sessions_.empty();
        } else {
            return !sessions_[1].empty() && !interrupter_;
        }
    }

    std::string interlocutor() const {
        if (multiuser_) {
            return "";
        }
        std::string meId = wApp->sessionId();
        if (meId == sessions_[0]) {
            return sessions_[1];
        } else if (meId == sessions_[1]) {
            return sessions_[0];
        } else {
            return "";
        }
    }

    bool canMakeMuc() const {
        return wApp && reserved_ && !multiuser_ &&
               sessions_.size() == 2 && sessions_[1].empty() &&
               wApp->sessionId() == sessions_[0];
    }

    void makeMuc() {
        if (canMakeMuc()) {
            multiuser_ = true;
            sessions_.resize(1);
        }
    }

    void post(const boost::function<void()>& f) {
        std::string meId = wApp->sessionId();
        BOOST_FOREACH (const std::string& user, sessions_) {
            if (!user.empty() && user != meId) {
                WServer::instance()->post(user, f);
            }
        }
    }
};

typedef boost::shared_ptr<Chat> ChatPtr;

enum {
    AUTHOR_COL,
    TEXT_COL,
    TIME_COL
};

const int COLUMNS = 3;

const Messages empty_messages_;

WString filterMessage(const WString& text);

struct MessagesReader {
    const Messages* messages_;
    WApplication* me_;
    ChatPtr chat_;
    int begin_;

    MessagesReader():
        messages_(&empty_messages_), me_(wApp), begin_(0) {
    }

    void setMessages(const Messages* messages) {
        messages_ = messages;
        begin_ = messages->size();
        updateReader();
    }

    void resetMessages() {
        setMessages(&empty_messages_);
    }

    bool canRead(int i) const {
        if (!chat_) {
            return false;
        } else if (chat_->multiuser_ && i < begin_) {
            return false;
        } else {
            return true;
        }
    }

    bool me(const Message& message) const {
        return message.author_ == me_;
    }

    virtual void updateReader() {
    }

    WString authorText(const Message& message) const {
        return me(message) ? TR("me") :
               message.author_ ? "-&gt;" : "";
    }

    WWidget* renderAuthor(int i) const {
        const Message& m = messages_->at(i);
        WText* a = new WText(authorText(m));
        if (m.author_) {
            a->addStyleClass("author");
            if (chat_->multiuser_) {
                WColor c = colorOf(m.author_);
                a->decorationStyle().setBackgroundColor(c);
            } else {
                a->addStyleClass(me(m) ? "me-author" :
                                 "int-author");
            }
        }
        return a;
    }

    WWidget* renderText(int i) const {
        const Message& m = messages_->at(i);
        WText* text;
        if (m.author_) {
            text = new WText(filterMessage(m.text_));
        } else {
            // system message
            text = new WText(m.text_);
        }
        text->addStyleClass(me(m) ? "me-text" : "int-text");
        return text;
    }

    WWidget* renderTime(int i) const;

    void makeRow(WTableRow* r, int i) const {
        r->elementAt(AUTHOR_COL)->setStyleClass("author-col");
        r->elementAt(TEXT_COL)->setStyleClass("text-col");
        r->elementAt(TIME_COL)->setStyleClass("time-col");
        WWidget* author = renderAuthor(i);
        r->elementAt(AUTHOR_COL)->addWidget(author);
        r->elementAt(TEXT_COL)->addWidget(renderText(i));
        WWidget* time_widget = renderTime(i);
        if (time_widget) {
            r->elementAt(TIME_COL)->addWidget(time_widget);
        }
    }

    void makeDiv(WTemplate* t, int i) const {
        WWidget* time_widget = renderTime(i);
        if (time_widget) {
            t->setTemplateText("${author} ${time} "
                               "${text}<br/>");
        } else {
            t->setTemplateText("${author} ${text}<br/>");
        }
        WContainerWidget* author_col = new WContainerWidget;
        author_col->addStyleClass("author-col left-div");
        author_col->addWidget(renderAuthor(i));
        t->bindWidget("author", author_col);
        t->bindWidget("text", renderText(i));
        if (time_widget) {
            WContainerWidget* time_col = new WContainerWidget;
            time_col->addStyleClass("time-col right-div");
            time_col->addWidget(time_widget);
            t->bindWidget("time", time_col);
        }
    }
};

const int HTML_ROWS = 25;
const int PENDING_TIMEOUT = 60; // seconds
const int TYPING_TIMEOUT = 3000;
const int TYPING_TIMEOUT_2 = 2500;
const int BLUR_TIMEOUT = 20000;
const int HELLO_TIMEOUT = 20000;
const int RND_LENGTH = 5;
const int RND_LENGTH_LONG = 16;

WString ajax_img_("<img src='/resources/ajax-loading.gif'/>"
        "<br/><br/>");

const char* EMOTIONS[] = {
    ":)", "smile",
    ":-)", "smile",
    ":D", "laughing",
    ":(", "frown",
    ":-(", "frown",
    ":p", "tongue-out",
    ":-p", "tongue-out",
    ":P", "tongue-out",
    ":-P", "tongue-out",
    ";)", "wink",
    ";-)", "wink",
    ":*", "kiss",
    ":-*", "kiss",
};

const int EMOTIONS_SIZE = 13;
const char* EMOTIONS_FILE = "/img/emotions/smiley-%1%.gif";
const char* EMOTIONS_TAG = "<img src='%1%' title='%2%'/>";

std::string emotionFile(int i) {
    const char* emotion = EMOTIONS[i * 2 + 1];
    boost::format format((EMOTIONS_FILE));
    return (format % emotion).str();
}

std::string emotionPattern(int i) {
    return EMOTIONS[i * 2];
}

std::string emotionTag(int i) {
    boost::format format((EMOTIONS_TAG));
    return (format % emotionFile(i) %
            emotionPattern(i)).str();
}

void addEmotions(std::string& t) {
    for (int i = 0; i < EMOTIONS_SIZE; i++) {
        using namespace boost::algorithm;
        replace_all(t, emotionPattern(i), emotionTag(i));
    }
}

void addLinks(std::string& t) {
    boost::regex http_re("\\bhttps?://(([^w]|w[^w]|ww[^w])"
                         "\\S+)");
    std::string http_f("<a href='$&' target='_blank'>$1</a>");
    t = boost::regex_replace(t, http_re, http_f);
    boost::regex www_re("\\b(https?://)?(www\\.\\S+)");
    std::string www_f("<a href='http://$2' "
                      "target='_blank'>$2</a>");
    t = boost::regex_replace(t, www_re, www_f);
}

void styleTable(WWidget* t) {
    t->addStyleClass("chat-table");
}

void styleRow(WTableRow* r) {
}

void checkSendInHTML0();

class SmileyPreloader : public WContainerWidget {
public:
    SmileyPreloader() {
        resize(0, 0);
        for (int i = 0; i < EMOTIONS_SIZE; i++) {
            WImage* sm = new WImage(this);
            sm->resize(0, 0);
            sm->setImageLink(emotionFile(i));
        }
        WImage* favicon2 = new WImage(this);
        favicon2->resize(0, 0);
        favicon2->setImageLink("/img/favicon2.ico");
    }
};

class HTMLChat : public WViewWidget, public MessagesReader {
public:
    HTMLChat():
        smileys_preloaded_(false) {
    }

    WWidget* renderView() {
        checkSendInHTML0();
        WTable* t = new WTable;
        styleTable(t);
        int rows = std::min(int(messages_->size()),
                            HTML_ROWS);
        int shift = messages_->size() - rows;
        for (int row = 0; row < rows; row++) {
            int i = row + shift;
            if (canRead(i)) {
                makeRow(t->rowAt(row), i);
            }
        }
        if (!smileys_preloaded_ && has_emotions_) {
            smileys_preloaded_ = true;
            WContainerWidget* c = t->elementAt(0, TEXT_COL);
            c->addWidget(new SmileyPreloader);
        }
        return t;
    }

    void updateReader() {
        update();
    }

private:
    bool smileys_preloaded_;
};

std::string chatRow(const MessagesReader* reader, int i);

class SmileyPreloaderView : public WViewWidget {
public:
    WWidget* renderView() {
        return new SmileyPreloader;
    }
};

void escapeJsArg(std::string& t) {
    using namespace boost::algorithm;
    replace_all(t, "\\", "\\\\");
    replace_all(t, "'", "\\'");
    t = boost::regex_replace(t, boost::regex("\\s"), " ");
    t = "'" + t + "'";
}

class JSChat : public WContainerWidget,
    public MessagesReader {
public:
    JSChat():
        last_rows_(0) {
        if (has_emotions_) {
            addWidget(new SmileyPreloaderView);
        }
    }

    void clearView() {
        last_rows_ = 0;
        clearMesages();
    }

    void clearMesages() {
        std::string idi = "'" + id() + "'";
        DO_JS("clearMesages(" + idi + ");");
    }

    void updateReader() {
        if (last_rows_ > messages_->size()) {
            last_rows_ = 0;
            clearMesages();
        }
        int begin = last_rows_;
        int end = messages_->size();
        for (int i = begin; i < end; i++) {
            if (canRead(i)) {
                std::string idi = "'" + id() + "'";
                std::string t = chatRow(this, i);
                escapeJsArg(t);
                DO_JS("addMessage(" + idi + ", " + t + ");");
            }
        }
        last_rows_ = messages_->size();
    }

private:
    int last_rows_;
};

class AChat : public WScrollArea, public MessagesReader {
public:
    AChat():
        progress_(false) {
        HTMLChat* html_chat = new HTMLChat;
        setWidget(html_chat);
        reader_ = html_chat;
        addStyleClass("chat-scroll");
    }

    void clearView() {
        JSChat* js_chat = dynamic_cast<JSChat*>(reader_);
        if (js_chat) {
            js_chat->clearView();
        }
    }

    void updateReader() {
        if (!progress_ && wApp->environment().ajax()) {
            delete takeWidget();
            JSChat* js_chat = new JSChat;
            setWidget(js_chat);
            reader_ = js_chat;
            progress_ = true;
        }
        reader_->messages_ = messages_;
        reader_->chat_ = chat_;
        reader_->begin_ = begin_;
        reader_->updateReader();
        DO_JS("setTimeout(function() {" +
              jsRef() + ".scrollTop=" +
              jsRef() + ".scrollHeight}, 10);");
    }

private:
    MessagesReader* reader_;
    bool progress_;
};

class LogGenerator : public WResource, public MessagesReader {
public:
    void handleRequest(const Http::Request& request,
                       Http::Response& resp) {
        if (!chat_) {
            return;
        }
        int size = messages_->size();
        for (int i = 0; i < size; i++) {
            const Message& message = messages_->at(i);
            if (canRead(i) && !message.text_.empty()) {
                resp.out() << timeOf(message) << ' ';
                WString user = authorText(message);
                if (user == "-&gt;") {
                    if (chat_->multiuser_) {
                        user = "User";
                        user += colorOf(message.author_);
                    } else {
                        user = "->";
                    }
                }
                resp.out() << user.toUTF8();
                resp.out() << ": " << message.text_.toUTF8();
                resp.out().put('\r').put('\n');
            }
        }
    }

    void updateReader();
};

const char* GOOD_TAGS = "iubs";
const int GOOD_TAGS_SIZE = 4;

WString filterMessage(const WString& text) {
    std::string t = Utils::htmlEncode(text).toUTF8();
    std::string search1(("&lt;.&gt;"));
    std::string replace1(("<.>"));
    std::string search2(("&lt;/.&gt;"));
    std::string replace2(("</.>"));
    for (int i = 0; i < GOOD_TAGS_SIZE; i++) {
        char tag = GOOD_TAGS[i];
        search1[4] = tag;
        replace1[1] = tag;
        search2[5] = tag;
        replace2[2] = tag;
        using namespace boost::algorithm;
        replace_all(t, search1, replace1);
        replace_all(t, search2, replace2);
    }
    if (has_emotions_) {
        addEmotions(t);
    }
    addLinks(t);
    return WString::fromUTF8(t);
}

typedef std::map<std::string, ChatPtr> Str2Chat;
Str2Chat free_chats_;
CHAT_MUTEX free_chat_mutex_;
Str2Chat reserved_chats_;
CHAT_MUTEX reserved_chat_mutex_;

std::string currentLocale() {
    std::string locale = "en";
    WApplication* app = wApp;
    if (app) {
        locale = app->locale();
        if (locale.length() < 2) {
            locale = "en";
        } else if (locale.length() > 2) {
            locale.resize(2);
        }
    }
    return locale;
}

ChatPtr& free_chat() {
    return free_chats_[currentLocale()];
}

ChatPtr no_pending_;

ChatPtr& longPending() {
    WDateTime now = WDateTime::currentDateTime();
    BOOST_FOREACH (Str2Chat::value_type& l_c, free_chats_) {
        ChatPtr& c = l_c.second;
        if (c && c->created_.secsTo(now) > PENDING_TIMEOUT) {
            return c;
        }
    }
    return no_pending_;
}

class TaTApp : public WApplication {
public:
    TaTApp(const WEnvironment& env):
        WApplication(env),
        sound_(0),
        input_changed_(this, "input_changed"),
        join_reserved_(this, "join_reserved"),
        focus_changed_(this, "focus_changed"),
        new_message_(this, "new_message"),
        tz_signal_(this, "tz_signal"),
        prev_length_(0),
        time_shift_(0),
        chat_container_initialized_(false),
        focus_(true),
        mobile_(false),
        constructor_(true),
        has_join_reserved_(false) {

        // It is needed to fix jQuery issue TypeError: url.indexOf is not a function
        // See https://redmine.webtoolkit.eu/boards/2/topics/13573?r=14307#message-14307
        requireJQuery("jquery.min.js");

        int active_users_number;
        {
            CHAT_LOCK lock(users_number_mutex_);
            active_users_.insert(sessionId());
            active_users_number = active_users_.size();
        }
        bool mob_wt = env.agentIsIEMobile() ||
                      env.agentIsMobileWebKit();
        using namespace boost::algorithm;
        std::string ua = to_lower_copy(env.userAgent());
        bool mob_ua = ua.find("mobile") != std::string::npos;
        bool ie6 = ua.find("msie 6") != std::string::npos;
        bool ie7 = ua.find("msie 7") != std::string::npos;
        bool ie8 = ua.find("msie 8") != std::string::npos;
        one_button_ = ie6 || ie7 || ie8;
        WCssStyleSheet& sheet = styleSheet();
        if (mob_wt || mob_ua) {
            mobile_ = true;
            sheet.addRule(".author-col", "width: 40px;");
        } else {
            sheet.addRule(".author-col", "width: 55px;");
        }
        sheet.addRule("body", "background: #F9FAFB;"
                      "background-image: "
                      "url(/img/geometry2.png) !important;"
                      "color: #333;"
                      "line-height: 1.428571429;"
                      "font-size: 14px;"
                      "font-family: 'Helvetica Neue',"
                      "Helvetica,Arial,sans-serif;");
        sheet.addRule(".appendix",
                      "padding-right: 10px !important;");
        sheet.addRule("div.Wt-loading",
                      "background: #FFF1A8; color: #AA9F67;");
        sheet.addRule(".btn-primary",
                      "background: #3276B1; color: white;");
        sheet.addRule(".btn-success",
                      "background: #47A447; color: white;");
        sheet.addRule("a", "text-decoration: none;");
        sheet.addRule("h1", "font-weight: 500;"
                      "margin-bottom: 10px;"
                      "margin-top: 20px;"
                      "font-size: 36px; line-height: 1.1;");
        sheet.addRule("*", "box-sizing: border-box;");
        bs_.setVersion(WBootstrapTheme::Version3);
        setTheme(&bs_);
        setTitle(TR("title"));
        ls_ = gls_;
        // hack
        while (!localizedStrings_->items().empty()) {
            WLocalizedStrings* ls;
            ls = localizedStrings_->items()[0];
            localizedStrings_->remove(ls);
            delete ls;
        }
        localizedStrings_->add(ls_->create_localized_strings());
        useStyleSheet("/resources/font-awesome"
                      "/css/font-awesome.min.css");
        //
        WVBoxLayout* root_l = vbox(root());
        root_l->setContentsMargins(0, 3, 0, 0);
        root_l->addWidget(&stacked_);
        stacked_.addStyleClass("main-stack");
        //
        WVBoxLayout* wel_l = vbox(&welcome_);
        WHBoxLayout* nav_l = hbox();
        nav_l->addWidget(&langs_, 1, AlignRight);
        addLangs();
        //
        rnd_ = internalPathNextPart("/");
        bool join_reserved = false;
        if (!rnd_.empty()) {
            CHAT_LOCK lock(reserved_chat_mutex_);
            typedef Str2Chat::iterator It;
            It it = reserved_chats_.find(rnd_);
            if (it != reserved_chats_.end() &&
                    it->second) {
                join_reserved = true;
                chat_ = it->second;
                CHAT_LOCK lock2(chat_->mutex_);
                if (!chat_->multiuser_) {
                    reserved_chats_.erase(it);
                }
            }
        }
        reserve_.setText(TR("reserve"));
        reserve_.addStyleClass("btn-success");
        reserve_.setMinimumSize(150, 35);
        if (!join_reserved) {
            reserve_.clicked().connect(this,
                                       &TaTApp::reserveChat);
        } else {
            joinReserved();
        }
        //
        wel_l->addLayout(nav_l);
        wel_l->addWidget(&main_scroll_, 2);
        stacked_.addWidget(&welcome_);
        stacked_.addWidget(&chat_c_);
        main_scroll_.setHorizontalScrollBarPolicy(
            WScrollArea::ScrollBarAlwaysOff);
        main_scroll_.setWidget(&main_);
        main_.addFunction("tr", &WTemplate::Functions::tr);
        WString txt = TR("main");
        if (join_reserved) {
            txt += TR("redirect-reserved")
                   .arg(rnd_).arg(sessionId());
        }
        main_.setTemplateText(txt, XHTMLUnsafeText);
        main_.bindInt("active-users", active_users_number);
        main_.bindString("top-space", "<br/><br/>");
        main_.bindWidget("start", &start_);
        main_.bindWidget("reserve", &reserve_);
        main_.bindWidget("more", &more_c_);
        main_.bindWidget("appendix", &appendix_);
        if (mobile_) {
            main_.bindString("top-space", "<br/>");
        }
        if (one_button_) {
            reserve_.hide();
        }
        more_i_.setText("<br/>"
                        "<i class='fa fa-angle-double-down'>"
                        "</i>");
        more_.setText(TR("more"));
        more_c_.addWidget(&more_);
        more_c_.addWidget(&more_i_);
        more_.clicked().connect(this, &TaTApp::showMore);
        appendix_.addFunction("tr",
                              &WTemplate::Functions::tr);
        appendix_.setTemplateText(TR("main-appendix"));
        appendix_.hide();
        appendix_.addStyleClass("appendix");
        start_.setText(TR("start"));
        start_.addStyleClass("btn-primary");
        start_.setMinimumSize(150, 35);
        start_.clicked().connect(this, &TaTApp::startChat);
        //
        setTabOrders();
        //
        addMetaHeader("viewport", "width=device-width, "
                      "initial-scale=1.0, maximum-scale=1.0, "
                      "user-scalable=0");
        //
        internalPathChanged().connect(this,
                                      &TaTApp::pathChanged);
        if (env.agentIsSpiderBot()) {
            showMore();
            langs_.show();
        }
        doJavaScript("window.alert = window.console && "
                     "window.console.log || $.noop;");
        constructor_ = false;
    }

    void enableAjax() {
        WApplication::enableAjax();
        load_container_.setSingleShot(true);
        load_container_.setInterval(5000);
        load_container_.start();
        load_container_.timeout().connect(boost::bind(
                    &TaTApp::fillChatContainer, this));
    }

    WHBoxLayout* hbox(WWidget* parent = 0) {
        WHBoxLayout* box = new WHBoxLayout(parent);
        if (mobile_) {
            box->setContentsMargins(2, 0, 0, 0);
        }
        return box;
    }

    WVBoxLayout* vbox(WWidget* parent = 0) {
        WVBoxLayout* box = new WVBoxLayout(parent);
        if (mobile_) {
            box->setContentsMargins(2, 0, 0, 0);
        }
        return box;
    }

    void setTabOrders() {
        update_.setTabIndex(4);
        download_.setTabIndex(100);
        input_.setTabIndex(1);
        start_.setTabIndex(1);
        more_.setTabIndex(6);
        send_.setTabIndex(2);
        stop_.setTabIndex(5);
        start2_.setTabIndex(1);
        leave_.setTabIndex(5);
    }

    void fillChatContainer() {
        if (chat_container_initialized_) {
            return;
        }
        chat_container_initialized_ = true;
        enableUpdates();
        require("/js/changeFavicon.js", "changeFavicon");
        require("/js/newMessages-12.js", "setAltered");
        useStyleSheet("/css/style-9.css");
        if (!sound_) {
            sound_ = new WSound("/sound/message2.mp3", this);
        }
        bool ajax = environment().ajax();
        WVBoxLayout* chat_cl = vbox(&chat_c_);
        WHBoxLayout* top_l = hbox();
        chat_cl->addLayout(top_l);
        top_l->addWidget(&scroll_down_, 0, AlignLeft);
        scroll_down_.addStyleClass("scroll-down");
        //
        WHBoxLayout* buttons_l0 = hbox();
        buttons_l0->addWidget(&update_);
        buttons_l0->addWidget(&make_muc_);
        buttons_l0->addWidget(&stop_);
        WVBoxLayout* buttons_l = vbox();
        buttons_l->addLayout(buttons_l0, 0);
        buttons_l->addStretch(1);
        make_muc_.setText(mucText());
        make_muc_.clicked().connect(this, &TaTApp::makeMuc);
        stop_.setText(stopText());
        update_.setImageLink("/img/reload.png");
        update_.clicked().connect(this,
                                  &TaTApp::doNothing);
        //
        if (mobile_) {
            WHBoxLayout* top_mobile_l = hbox();
            top_mobile_l->addStretch(1);
            top_mobile_l->addWidget(&download_);
            top_mobile_l->addLayout(buttons_l);
            top_l->addLayout(top_mobile_l, 1, AlignRight);
            chat_cl->addWidget(&state_);
            stop_.clicked().connect(this, &TaTApp::stopChat);
        } else {
            top_l->addWidget(&state_, 1, AlignCenter);
            top_l->addWidget(&download_, 0, AlignRight);
            top_l->addLayout(buttons_l, 0, AlignRight);
            stop_.clicked().connect(this, &TaTApp::stopChat0);
        }
        if (ajax) {
            update_.hide();
        }
        if (mobile_) {
            make_muc_.hide();
        }
        //
        WVBoxLayout* spacer_l = vbox();
        chat_cl->addLayout(spacer_l, 1);
        spacer_l->addStretch(1);
        spacer_l->addWidget(&achat_);
        chat_cl->addWidget(&manager_);
        manager_.addWidget(&waiting_);
        manager_.addWidget(&inputs_);
        manager_.addWidget(&final_);
        //
        WHBoxLayout* waiting_l = hbox(&waiting_);
        waiting_l->addWidget(&waiting_text_, 1);
        waiting_l->addStretch(1);
        if (!mobile_) {
            waiting_l->addWidget(&leave_, 0, AlignBottom);
            leave_.setMaximumSize(WLength::Auto, 40);
            leave_.setText(TR("leave"));
            leave_.clicked().connect(this, &TaTApp::stopChat);
        }
        waiting_text_.setText(ajax_img_ + TR("waiting-text"));
        state_.addStyleClass("state");
        download_.setText(TR("download"));
        download_.setLink(&log_);
        download_.setTarget(TargetNewWindow);
        download_.addStyleClass("download-log");
        input_.addStyleClass("chat-input");
        if (ajax) {
            input_.setMinimumSize(WLength::Auto, 30);
        } else {
            input_.setMinimumSize(200, 30);
            input_.resize(300, 30);
        }
        send_.addStyleClass("btn-success");
        send_.setText(TR(mobile_ ? "send-mobile" : "send"));
        if (ajax) {
            connectJS();
        } else {
            send_.clicked().connect(this, &TaTApp::sendChat);
            input_.enterPressed().connect(this,
                                          &TaTApp::sendChat);
        }
        WHBoxLayout* inputs_l = hbox();
        inputs_l->setContentsMargins(0, 0, 0, 0);
        inputs_l->addWidget(&input_, 1);
        if (!ajax || mobile_) {
            inputs_l->addWidget(&send_);
        }
        inputs_.setLayout(inputs_l);
        updateStop();
        //
        WHBoxLayout* final_l = hbox(&final_);
        final_l->addWidget(&start2_);
        final_l->addWidget(&goto_main_);
        final_l->addStretch(1);
        start2_.addStyleClass("btn-primary");
        start2_.setText(TR("start-new"));
        start2_.clicked().connect(this, &TaTApp::startChat);
        goto_main_.setText(TR("goto-main"));
        goto_main_.clicked().connect(this, &TaTApp::gotoMain);
        if (mobile_ || !ajax) {
            start2_.hide();
        }
        //
        tz_signal_.connect(this, &TaTApp::setTzShift);
        DO_JS(tz_signal_.createCall(
                  "(new Date()).getTimezoneOffset()"));
        //
        DO_JS("activateChatScroll();setMaxWidth();");
    }

    void setTzShift(int shift) {
        shift *= (-60);
        shift = std::max(shift, -24 * 60 * 60);
        shift = std::min(shift, 24 * 60 * 60);
        time_shift_ = shift;
        achat_.clearView();
        achat_.updateReader();
        log_.updateReader();
    }

    WDateTime localTime(const WDateTime& dt) const {
        return dt.addSecs(time_shift_);
    }

    void updateStop() {
        bool wait = (manager_.currentWidget() == &waiting_);
        bool inputs = (manager_.currentWidget() == &inputs_);
        if (one_button_) {
            update_.setHidden(!wait);
            leave_.hide();
            stop_.hide();
        } else {
            stop_.setHidden(!inputs);
        }
    }

    WString stopText() const {
        if (!mobile_) {
            return TR("stop");
        } else {
            return "X";
        }
    }

    WString mucText() const {
        return TR("muc-make");
    }

    void connectJS() {
        new_message_.connect(this, &TaTApp::newMessage);
        std::string js  = "var t = $('.chat-input').val();";
        // limit message length to 10k,
        // because large messages (e.g., 40k)
        // causes JS errors in Firefox
        // if WebSocket is used
        js += "t = t.substring(0, 10000);";
        js += new_message_.createCall("t");
        js += "clearInput();";
        js = "function(o, e) { " + js + "}";
        declareJavaScriptFunction("sendM", js);
        std::string func = javaScriptClass() + ".sendM";
        send_.clicked().connect(func);
        input_.enterPressed().connect(func);
        //
        // TODO save connection and disconnect in MUC
        input_changed_.connect(this, &TaTApp::inputChanged);
        DO_JS("window.last_ = new Date().getTime();");
        input_.keyWentUp().connect("function (o, e) {"
                                   "var t = new Date()."
                                   "getTime();"
                                   "if (" + input_.jsRef() +
                                   ".value == '' || "
                                   "t - window.last_ > " +
                                   TO_S(TYPING_TIMEOUT_2) +
                                   ") {" +
                                   input_changed_.createCall()
                                   + "window.last_ = t;}}");
        // TODO save connection and disconnect in MUC
        input_.changed().connect(this, &TaTApp::inputChanged);
        focus_changed_.connect(this, &TaTApp::focusChanged);
        // $(window).focus or $(window).focus.bind('focus')
        // does not work in Chrome
        DO_JS("window.onfocus = function() {try{"
              "if (window.in_focus_ == false) {" +
              focus_changed_.createCall("1") + ";"
              "window.in_focus_ = true;"
              "}}catch(e) { } };");
        DO_JS("$(window).mousemove(function() {try{"
              "if (window.in_focus_ == false) {" +
              focus_changed_.createCall("1") + ";"
              "window.in_focus_ = true;"
              "}}catch(e) { } });");
        DO_JS("window.onblur = function() {try{"
              "if (window.in_focus_ == true) {" +
              focus_changed_.createCall("0") + ";"
              "window.in_focus_ = false;"
              "}}catch(e) { } };");
    }

    void focusChanged(bool focus) {
        focus_ = focus;
    }

    void showMore() {
        bool mp = (stacked_.currentWidget() == &welcome_);
        if (mp) {
            langs_.hide();
            more_c_.hide();
            appendix_.show();
            main_.bindString("top-space", "");
            updateLayout();
        }
    }

    ~TaTApp() {
        {
            CHAT_LOCK lock(users_number_mutex_);
            active_users_.erase(sessionId());
        }
        delete sound_;
        sound_ = 0;
    }

    void finalize() {
        stopChat();
        stacked_.setCurrentIndex(0);
        manager_.setCurrentIndex(0);
        updateLayout();
    }

    void addLangs() {
        addLang("en");
        addLang("ru");
        addLang("de");
        addLang("es");
    }

    void addLang(std::string lang) {
        WAnchor* a = new WAnchor("?lang=" + lang);
        if (mobile_) {
            a->setText(lang);
        } else {
            a->setText(TR(lang));
        }
        if (langs_.count()) {
            langs_.addWidget(new WText(" / "));
        }
        langs_.addWidget(a);
        const std::string* get_lang;
        get_lang = environment().getParameter("lang");
        if (get_lang) {
            if (*get_lang == lang) {
                setLocale(lang);
                lang_ = lang;
            }
        }
    }

    void updateChat() {
        if (!chat_) {
            return;
        }
        achat_.updateReader();
    }

    void postJoining() {
        if (chat_) {
            if (chat_->multiuser_) {
                WString color = colorOf(wApp);
                newMessage2(TR("muc-join").arg(color), 0);
            } else {
                chat_->post(&TaTApp::acceptJoining);
            }
        }
    }

    static void acceptJoining() {
        TaTApp* a = DOWNCAST<TaTApp*>(wApp);
        if (a->chat_ && a->chat_->active() &&
                a->chat_->messages_.size() <= 1) {
            a->startDialog();
            a->triggerUpdate();
        }
    }

    void startDialog() {
        fillChatContainer();
        if (!chat_) {
            return;
        }
        download_.show();
        if (!chat_->canMakeMuc()) {
            make_muc_.hide();
        }
        if (!chat_->multiuser_) {
            setConfirmCloseMessage(TR("confirm-close"));
            setState(TR("hello"));
            setStateTimeout("", HELLO_TIMEOUT);
        }
        manager_.setCurrentWidget(&inputs_);
        updateStop();
        updateLayout();
        input_.setText("");
        input_.setFocus();
        playSound();
    }

    void playSound() {
        if (sound_) {
            sound_->play();
        }
        DO_JS("window.new_messages_ = true;");
    }

    static void acceptMessage() {
        TaTApp* a = DOWNCAST<TaTApp*>(wApp);
        if (a->chat_ && a->chat_->active()) {
            a->updateChat();
            a->setState("");
            if (!a->focus_) {
                a->playSound();
            }
            a->triggerUpdate();
        }
    }

    static void acceptTyping() {
        TaTApp* a = DOWNCAST<TaTApp*>(wApp);
        if (a->chat_ && a->chat_->active() &&
                !a->chat_->multiuser_) {
            a->setState(TR("typing"));
            a->typedTimeout();
            a->triggerUpdate();
        }
    }

    static void acceptDeleting() {
        TaTApp* a = DOWNCAST<TaTApp*>(wApp);
        if (a->chat_ && a->chat_->active() &&
                !a->chat_->multiuser_) {
            a->setState(TR("deleting"));
            a->typedTimeout();
            a->triggerUpdate();
        }
    }

    void typedTimeout() {
        setStateTimeout(TR("typed"), TYPING_TIMEOUT);
    }

    void setState(const WString& state) {
        if (chat_ && chat_->multiuser_) {
            state_.setText("&nbsp;");
        } else if (state_.text() == state) {
            // force update state changed in JS
            state_.setText(state + " ");
        } else if (state.empty()) {
            state_.setText("&nbsp;");
        } else {
            state_.setText(state);
        }
        clearTimeout();
    }

    void clearTimeout() {
        if (chat_ && !chat_->multiuser_) {
            DO_JS("try{clearTimeout(window.timeout_);}"
                  "catch(e) { };");
        }
    }

    void setStateTimeout(const WString& state, int ms) {
        if (chat_ && !chat_->multiuser_) {
            clearTimeout();
            DO_JS("window.timeout_=setTimeout(function(){" +
                  state_.jsRef() + ".innerHTML='" +
                  state.toUTF8() + "&nbsp;'}," +
                  TO_S(ms) + ");");
        }
    }

    void updateLayout() {
        DO_JS("setTimeout(window.onresize, 100);");
        DO_JS("setTimeout(window.onresize, 1000);");
        DO_JS("setTimeout(window.onresize, 3000);");
    }

    static void acceptEmpty() {
        TaTApp* a = DOWNCAST<TaTApp*>(wApp);
        if (a->chat_ && a->chat_->active()) {
            if (a->state_.text() != TR("hello")) {
                a->setState("");
                a->triggerUpdate();
            }
        }
    }

    void checkSendInHTML() {
        if (!input_.text().empty() && !environment().ajax()) {
            sendChat();
        }
    }

    void notify(const WEvent& e) {
        if (chat_) {
            // prevent deletion of locked chat_->mutex_
            ChatPtr chat = chat_;
            CHAT_LOCK lock(chat_->mutex_);
            WApplication::notify(e);
        } else {
            WApplication::notify(e);
        }
    }

    void startChat() {
        prev_length_ = 0;
        fillChatContainer();
        download_.hide();
        make_muc_.hide();
        if (chat_) {
            if (chat_->interrupter_ || chat_->multiuser_) {
                stopChat();
            } else {
                // too fast double click on "Start"
                return;
            }
        }
        {
            CHAT_LOCK lock(free_chat_mutex_);
            ChatPtr& fc = free_chat() ? : longPending();
            if (fc) {
                chat_ = fc;
                fc.reset();
                CHAT_LOCK lock2(chat_->mutex_);
                chat_->join();
                startDialog();
                postJoining();
            } else {
                chat_ = boost::make_shared<Chat>();
                chat_->sessions_[0] = sessionId();
                free_chat() = chat_;
                setState(TR("waiting"));
                manager_.setCurrentWidget(&waiting_);
                updateLayout();
            }
        }
        startChat1();
    }

    void reserveChat() {
        prev_length_ = 0;
        fillChatContainer();
        if (chat_) {
            stopChat();
        }
        rnd_ = WRandom::generateId(RND_LENGTH);
        {
            CHAT_LOCK lock(reserved_chat_mutex_);
            while (reserved_chats_.find(rnd_) !=
                    reserved_chats_.end()) {
                rnd_ = WRandom::generateId(RND_LENGTH_LONG);
            }
            chat_ = boost::make_shared<Chat>();
            chat_->reserved_ = true;
            reserved_chats_[rnd_] = chat_;
            chat_->sessions_[0] = sessionId();
        }
        std::string p = '/' + rnd_ + '/';
        setInternalPath(p, /* emit */ false);
        setState(TR("waiting"));
        manager_.setCurrentWidget(&waiting_);
        updateLayout();
        startChat1();
        addReservedMessage();
    }

    void pathChanged() {
        if (internalPathMatches("/" + rnd_ + "/join")) {
            joinReserved1();
        }
    }

    void joinReserved() {
        // JS
        join_reserved_.connect(this, &TaTApp::joinReserved1);
        doJavaScript(join_reserved_.createCall());
    }

    void joinReserved1() {
        if (has_join_reserved_) {
            return;
        }
        has_join_reserved_ = true;
        if (chat_) {
            chat_->join();
            if (!chat_->messages_.empty()) {
                chat_->messages_[0].text_ = "";
            }
            startDialog();
            startChat1();
            postJoining();
        }
    }

    void startChat1() {
        if (!chat_) {
            return;
        }
        fillChatContainer();
        updateStop();
        achat_.setMessages(&chat_->messages_);
        achat_.chat_ = chat_;
        stacked_.setCurrentWidget(&chat_c_);
        updateLayout();
        input_.setFocus();
        log_.setMessages(&chat_->messages_);
        log_.chat_ = chat_;
        unInterrupt();
    }

    void unInterrupt() {
        interrupt_attempt_ = false;
        stop_.setText(stopText());
    }

    void doNothing() {
    }

    void makeMuc() {
        make_muc_.hide();
        if (chat_) {
            chat_->makeMuc();
            if (chat_->active()) {
                startDialog();
            }
        }
    }

    void sendChat() {
        newMessage(input_.text());
        input_.setText("");
    }

    void newMessage(const WString& text) {
        unInterrupt();
        if (text.empty() || !chat_ || !chat_->active()) {
            return;
        }
        newMessage2(text, wApp);
    }

    void newMessage2(const WString& text,
                     WApplication* user) {
        Messages& messages = chat_->messages_;
        if (!messages.empty() &&
                messages.back().author_ == user &&
                messages.back().text_ == text) {
            return;
        }
        messages.push_back(Message());
        messages.back().time_ = WDateTime::currentDateTime();
        messages.back().author_ = user;
        messages.back().text_ = text;
        updateChat();
        chat_->post(&TaTApp::acceptMessage);
        input_.setFocus();
        prev_length_ = 0;
    }

    void addReservedMessage() {
        WApplication* author = 0;
        std::string host = environment().hostName();
        std::string url = host + '/' + rnd_;
        newMessage2(TR("reserved-welcome").arg(url), author);
        DO_JS("selectReserved();");
    }

    void stopChat0() {
        if (interrupt_attempt_) {
            stopChat();
        } else {
            interrupt_attempt_ = true;
            stop_.setText(TR("click-again"));
        }
    }

    void stopChat() {
        if (chat_) {
            chat_->leave();
            showFinal();
            if (chat_->multiuser_) {
                WString color = colorOf(wApp);
                newMessage2(TR("muc-leave").arg(color), 0);
            } else {
                chat_->post(&TaTApp::acceptFinal);
            }
            {
                CHAT_LOCK lock(free_chat_mutex_);
                if (chat_ == free_chat()) {
                    free_chat().reset();
                }
            }
            if (chat_->reserved_ && !chat_->multiuser_) {
                CHAT_LOCK lock(reserved_chat_mutex_);
                typedef Str2Chat::iterator It;
                It it = reserved_chats_.find(rnd_);
                if (it != reserved_chats_.end()) {
                    reserved_chats_.erase(it);
                }
            }
        }
    }

    void gotoMain() {
        if (lang_.empty()) {
            redirect("/");
        } else {
            redirect("/?lang=" + lang_);
        }
        quit();
    }

    void inputChanged() {
        if (!chat_ || !chat_->active() || chat_->multiuser_) {
            return;
        }
        int length = input_.text().value().length();
        if (length == 0) {
            chat_->post(&TaTApp::acceptEmpty);
        } else if (length > prev_length_) {
            chat_->post(&TaTApp::acceptTyping);
        } else if (length < prev_length_) {
            chat_->post(&TaTApp::acceptDeleting);
        }
        prev_length_ = length;
    }

    static void acceptFinal() {
        TaTApp* a = DOWNCAST<TaTApp*>(wApp);
        if (a->chat_ && a->chat_->interrupter_) {
            a->showFinal();
            a->triggerUpdate();
        }
    }

    void showFinal() {
        setConfirmCloseMessage("");
        manager_.setCurrentWidget(&final_);
        updateStop();
        updateLayout();
        start2_.setFocus();
        if (chat_) {
            if (chat_->interrupter_ == wApp) {
                setState(TR("final-me"));
            } else {
                setState(TR("final-int"));
            }
        }
    }

private:
    GLSPtr ls_;
    WBootstrapTheme bs_;
    WStackedWidget stacked_;
    WContainerWidget welcome_, chat_c_;
    WStackedWidget manager_;
    WContainerWidget waiting_, inputs_, final_;
    WScrollArea main_scroll_;
    WTemplate main_, appendix_;
    AChat achat_;
    WContainerWidget langs_;
    WAnchor scroll_down_;
    WText state_;
    WImage update_;
    WText waiting_text_;
    LogGenerator log_;
    WAnchor download_;
    WLineEdit input_;
    WSound* sound_;
    WTimer load_container_;
    JSignal<> input_changed_, join_reserved_;
    JSignal<bool> focus_changed_;
    JSignal<WString> new_message_;
    JSignal<int> tz_signal_;
    WPushButton start_, send_, stop_, start2_, leave_;
    WContainerWidget more_c_;
    WText more_i_;
    WAnchor more_;
    WPushButton reserve_, goto_main_, make_muc_;
    ChatPtr chat_;
    std::string rnd_;
    std::string lang_;
    int prev_length_;
    int time_shift_;
    bool interrupt_attempt_ : 1;
    bool chat_container_initialized_ : 1;
    bool focus_ : 1;
    bool constructor_ : 1;
    bool one_button_ : 1;
    bool has_join_reserved_ : 1;
public:
    bool mobile_ : 1;
};

bool isMobile() {
    if (wApp) {
        TaTApp* a = DOWNCAST<TaTApp*>(wApp);
        if (a) {
            return a->mobile_;
        }
    }
    return false;
}

WString timeOf(const Message& m) {
    if (m.text_.empty()) {
        return "";
    }
    if (wApp) {
        TaTApp* a = DOWNCAST<TaTApp*>(wApp);
        WDateTime local = a->localTime(m.time_);
        return local.toString("hh:mm");
    } else {
        return m.time_.toString("hh:mm");
    }
}

bool equalTime(const Message& a, const Message& b) {
    return a.time_.time().minute() == b.time_.time().minute();
}

WWidget* MessagesReader::renderTime(int i) const {
    const Message& m = messages_->at(i);
    if (i > 0) {
        const Message& pr = messages_->at(i - 1);
        if (pr.author_ == m.author_ && equalTime(m, pr)) {
            return 0;
        }
    }
    return new WText(timeOf(m));
}

std::string chatRow(const MessagesReader* reader, int i) {
    std::stringstream ss;
    if (isMobile()) {
        WTemplate t;
        styleTable(&t);
        reader->makeDiv(&t, i);
        t.htmlText(ss);
        return ss.str();
    } else {
        WTable t;
        styleTable(&t);
        WTableRow* row = t.rowAt(0);
        reader->makeRow(row, i);
        t.htmlText(ss);
        return ss.str();
    }
}

void LogGenerator::updateReader() {
    TaTApp* a = DOWNCAST<TaTApp*>(wApp);
    WDateTime now = WDateTime::currentDateTime();
    suggestFileName(a->localTime(now).toString() + ".txt");
}

void checkSendInHTML0() {
    TaTApp* a = DOWNCAST<TaTApp*>(wApp);
    if (a) {
        a->checkSendInHTML();
    }
}

WApplication* createTaTApp(const WEnvironment& env) {
    return new TaTApp(env);
}

bool fileExists(std::string path) {
    std::ifstream test(path.c_str());
    return test.good();
}

static void saveSessions() {
    WDateTime now = WDateTime::currentDateTime();
    std::string fname = now.toString().toUTF8() + ".txt";
    std::ofstream out(fname.c_str());
    int port = WServer::instance()->httpPort();
    CHAT_LOCK lock(users_number_mutex_);
    BOOST_FOREACH (const std::string& sId, active_users_) {
        out << sId << " http://127.0.0.1:" << port << ";\n";
    }
}

static void updateGls() {
    GlobalLocalizedStrings* new_gls;
    new_gls = new GlobalLocalizedStrings;
    new_gls->use(approot_ + "/text");
    new_gls->add_lang("ru");
    new_gls->add_lang("de");
    new_gls->add_lang("es");
    gls_.reset(new_gls);
}

static void onSignal(int) {
    saveSessions();
    updateGls();
}

int main(int argc, char** argv) {
    has_emotions_ = fileExists("docroot/" + emotionFile(0));
    signal(SIGUSR1, onSignal);
    WServer server;
    server.setServerConfiguration(argc, argv);
    server.addEntryPoint(Wt::Application, createTaTApp);
    approot_ = server.appRoot();
    updateGls();
    if (server.start()) {
        Wt::WServer::waitForShutdown();
        server.stop();
    }
}

