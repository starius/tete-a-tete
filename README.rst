===================================
Tete-a-tete, anonumous private chat
===================================

How does it work?
-----------------

This site connects random users anonymously.
To start chatting, click button "Random chat".
You can use smileys and simple formatting.
Press Enter to send a message.
You can talk to your friend as well.
Click "Chat with friend" and send generated URL to your friend.

Is it secure?
-------------

Identifying information is not recorded.
This site is not persistent, no chats are written to hard drive.
Logs remain in memory until communication is closed.

Enhancing anonymity and security
--------------------------------

Please note that site can work without JavaScript enabled
and is available from Tor Network.
Source code of the site is available at.

Mirror sites: https://kiset.org and
http://tetatl6umgbmtv27.onion

Installation on Debian
----------------------

It is recommended to link the site with static libraries.

Install Boost library and tinymce.

Build static Wt library (v. 3.3.3) and install it.

Then build tete-a-tete:

.. code-block:: console

    $ cmake .
    $ make

Executable is written to src/kiset

.. code-block:: console

    $ ./run.sh

SIGUSR1 updates translations (messages from XML files in
approot/ cached in the server globally) and saves sessions
to text file.

These options are useful for soft restart of the site,
which does not involve breaking existing sessions.

See file nginx/README.rst for instructions on how to
configure nginx for Tete-a-tete.

Useful links
------------

For more information please visit:
 * Source
     https://gitlab.com/starius/tete-a-tete
 * Report a bug or abuse:
     https://gitlab.com/starius/tete-a-tete/-/issues/new
 * Wt homepage:
     http://webtoolkit.eu/
 * Boost homepage:
     http://boost.org/

----

Boris Nagaev <bnagaev@gmail.com>

