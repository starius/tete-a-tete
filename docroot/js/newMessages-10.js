window.new_messages_ = false;
window.in_focus_ = true;
window.altered_ = false;
window.normal_title_ = document.title;

function setNormal() {
    window.altered_ = false;
    changeFavicon("/favicon.ico");
    document.title = window.normal_title_;
}

function setAltered() {
    if (document.title[0] != '-') {
        window.normal_title_ = document.title;
    }
    window.altered_ = true;
    changeFavicon("/img/favicon2.ico");
    document.title = '-- ' + window.normal_title_ + ' --';
}

setInterval(function() {
    if (window.new_messages_ && !window.in_focus_) {
        if (window.altered_) {
            setNormal();
        } else {
            setAltered();
        }
    }
    if (window.in_focus_) {
        window.new_messages_ = false;
    }
    if (!window.new_messages_ && window.altered_) {
        setNormal();
    }
}, 500);

function clearInput() {
    setTimeout(function() {
        $(".chat-input").val("");
    }, 1);
}

function removeBottomStretch() {
    try {
        var stack = $('.main-stack');
        // remove useless <tr> (parent of form stretch)
        stack.parent().parent().parent().children()[1].remove();
    } catch (e) {
    }
}

function activateChatScroll() {
    var chats = $(".chat-scroll");
    var brand = $(".scroll-down");
    var brand_orig = brand.html();
    chats.scroll(function() {
        var scrolled = chats.scrollTop() + chats.height();
        var max_scrolled = chats[0].scrollHeight;
        if (max_scrolled - scrolled > 200) {
            brand.html(brand_orig + '&nbsp;↓');
        } else {
            brand.html(brand_orig);
        }
    });
    brand.click(function() {
        chats[0].scrollTop = chats[0].scrollHeight;
    });
}

function setMaxWidth() {
    var width = $('.Wt-domRoot').css('width');
    $('.Wt-domRoot').css('max-width', width);
}
// http://stackoverflow.com/a/1173319
function selectText(container) {
    if (document.selection) {
        var range = document.body.createTextRange();
        range.moveToElementText(container);
        range.select();
    } else if (window.getSelection) {
        var range = document.createRange();
        range.selectNode(container);
        window.getSelection().addRange(range);
    }
}

function selectReserved() {
    $('.reserved-url').click(function() {
        selectText($('.reserved-url')[0]);
    });
}

function saveView0(view_id) {
    var view = Wt.WT.$(view_id);
    if (view) {
        view.setAttribute('id', view_id + '_copy');
        var dummy = document.createElement('div');
        view.appendChild(dummy);
        dummy.setAttribute('id', view_id);
        return true;
    }
    return false;
}

function saveView(view_id) {
    if (!saveView0(view_id)) {
        setTimeout(function() { saveView0(view_id) }, 100);
    }
}

