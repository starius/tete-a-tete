Here is example of nginx configuration
to run Tete-a-tete site (kiset.org) with following setup:
 * sevaral mirror domains (kiset.org, kiset.tk, tete-a-tete.tk
   and onion mirror in Tor network tetatl6umgbmtv27.onion).
 * SSL for site kiset.org
 * Web-Socket (requires nginx >= 1.3.13)
 * soft restart of site (transparent for old sessions)

Soft restart
============

 1. Kill running process of webapp with SIGUSR1.
    File with name of current date+time will be generated in
    work directory of the process.
    Move this file to old-sessions.
    Example of old-sessions is in file old-sessions.
    Port of old sessions in the example is 4580.

 2. Change port in nging.conf:
        default http://127.0.0.1:4581;
    and in run.sh to port
    of new instance of webapp (in example 4581).

 3. Run new instance of webapp process using ./run.sh

 4. Reload Nginx.

