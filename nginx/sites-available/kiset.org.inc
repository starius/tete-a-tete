    access_log off;

    location /resources {
        expires    7d;
        root /home/tat/tat/docroot;
    }

    location /css {
        expires    7d;
        root /home/tat/tat/docroot;
    }

    location /img {
        expires    7d;
        root /home/tat/tat/docroot;
    }

    location /js {
        expires    7d;
        root /home/tat/tat/docroot;
    }

    location /sound {
        expires    7d;
        root /home/tat/tat/docroot;
    }

    location /favicon.ico {
        root /home/tat/tat/docroot;
    }

    location /jquery.min.js {
        root /home/tat/tat/docroot;
    }

    location / {
        proxy_pass                  $backend_server;
        proxy_http_version          1.1;
        proxy_set_header Upgrade    $http_upgrade;
        proxy_set_header Connection $connection_upgrade;
        proxy_set_header Host       $host;
        charset                     utf-8;
    }

